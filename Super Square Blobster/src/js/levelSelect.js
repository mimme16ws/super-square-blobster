var levelSelect = function () {
    "use strict";
};

levelSelect.prototype = {
    create: function () {
        "use strict";
        this.game.add.sprite(0, 0, "third_level_background");
        var backButton = this.game.add.button(0, 0, "button_back", this.goBack, this, 1, 0);
        backButton.scale.setTo(0.8, 0.8);
        this.game.add.text(305, 30, "Levelauswahl", {
            fontSize: "28px",
            fill: "#fbb03b",
            stroke: "#000",
            strokeThickness: 8
        });
        var levelOneButton = this.game.add.button(80, 100, "selectLevelOne", this.playLevelOne, this, 1, 0);
        var levelTwoButton = this.game.add.button(440, 100, "selectLevelTwo", this.playLevelTwo, this, 1, 0);
        var levelThreeButton = this.game.add.button(80, 350, "selectLevelThree", this.playLevelThree, this, 1, 0);
        var levelFourButton = this.game.add.button(440, 350, "selectLevelFour", this.playLevelFour, this, 1, 0);
        levelOneButton.scale.setTo(0.35, 0.35);
        levelTwoButton.scale.setTo(0.35, 0.35);
        levelThreeButton.scale.setTo(0.35, 0.35);
        levelFourButton.scale.setTo(0.35, 0.35);
    },
    playLevelOne: function () {
        "use strict";
        this.game.state.start("levelOne");
    },
    playLevelTwo: function () {
        "use strict";
        this.game.state.start("levelTwo");
    },
    playLevelThree: function () {
        "use strict";
        this.game.state.start("levelThree");
    },
    playLevelFour: function () {
        "use strict";
        this.game.state.start("levelFour");
    },
    goBack: function () {
        "use strict";
        this.game.state.start("titleScreen");
    }
};
