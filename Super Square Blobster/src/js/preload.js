var preload = function () {
    "use strict";
};

preload.prototype = {
    /* Function loading all the assets used in the game. */
    preload: function () {
        "use strict";

        this.game.load.image("bg", "../../assets/bg7.png");
        this.game.load.image("bg2", "../../assets/background_2.png");
        this.game.load.image("highscorePic", "../../assets/highscorePicWithStars.png");
        this.game.load.image("nameInput", "../../assets/name_input.png");
        this.game.load.image("helpScreen", "../../assets/help_screen.png");
        this.game.load.image("highScoreLevelEnd", "../../assets/highscore_at_level_end.png");
        this.game.load.spritesheet("selectLevelOne", "../../assets/select_level_1_sprite.png", 800, 600);
        this.game.load.spritesheet("selectLevelTwo", "../../assets/select_level_2_sprite.png", 800, 600);
        this.game.load.spritesheet("selectLevelThree", "../../assets/select_level_3_sprite.png", 800, 600);
        this.game.load.spritesheet("selectLevelFour", "../../assets/select_level_4_sprite_new.png", 800, 600);

        /* First and Second Level Assets: */

        this.game.load.image("platform", "../../assets/cloud_medium.png");
        this.game.load.image("platformbig", "../../assets/cloud_big.png");
        this.game.load.image("platformsmall", "../../assets/cloud_small.png");
        this.game.load.image("star", "../../assets/stern.png");
        this.game.load.image("ground", "../../assets/platform_top.png");
        this.game.load.image("groundleft", "../../assets/platform_top_left.png");
        this.game.load.image("groundright", "../../assets/platform_top_right.png");
        this.game.load.image("groundall", "../../assets/platform_all.png");
        this.game.load.image("groundhigh", "../../assets/platform_high.png");

        this.game.load.image("goal", "../../assets/treasure_closed.png");

        this.game.load.image("spikes", "../../assets/stacheln_bottom.png");
        this.game.load.image("spikesalt", "../../assets/stacheln_bottom2.png");
        this.game.load.image("spikesleft", "../../assets/stacheln_left.png");
        this.game.load.image("spikesright", "../../assets/stacheln_right.png");

        this.game.load.spritesheet("sawblade", "../../assets/Zahn.png", 70, 70);
        this.game.load.spritesheet("goalnew", "../../assets/treasure_sprite.png", 64, 64);
        this.game.load.spritesheet("enemy", "../../assets/Enemy1.png", 64, 64);
        //this.game.load.image("title", "../../assets/SSB_Logo.png");
        this.game.load.image("title_new", "../../assets/SSB_Logo_neu.png");
        this.game.load.image("background_title", "../../assets/background_title.png");
        this.game.load.spritesheet("button_one", "../../assets/level_one_button.png", 189, 102);
        this.game.load.spritesheet("button_two", "../../assets/level_two_button.png", 189, 102);
        this.game.load.spritesheet("button_three", "../../assets/level_three_button_new.png", 189, 102);
        this.game.load.spritesheet("button_four", "../../assets/level_four_button_new.png", 189, 102);
        this.game.load.spritesheet("button_select", "../../assets/level_select_button_neu.png", 189, 102);
        this.game.load.spritesheet("button_highscores", "../../assets/highscores_button_neu.png", 189, 102);
        this.game.load.spritesheet("button_help", "../../assets/help_button_neu.png", 189, 102);
        this.game.load.spritesheet("button_back", "../../assets/back_button_new.png", 189, 102);
        this.game.load.image("speed", "../../assets/power_up_green2.png");
        this.game.load.image("shield", "../../assets/power_up_red2.png");
        this.game.load.image("slow", "../../assets/power_up_yellow2.png");

        /* Third and Fourth Level assets: */

        this.game.load.image("ground_2", "../../assets/platform_2_top.png");
        this.game.load.image("ground_2_all", "../../assets/platform_2_all.png");
        this.game.load.image("ground_2_left", "../../assets/platform_2_left.png");
        this.game.load.image("ground_2_right", "../../assets/platform_2_right.png");

        this.game.load.image("platform_2_small", "../../assets/platform_level_3_small.png");
        this.game.load.image("platform_2_medium", "../../assets/platform_level_3_medium.png");
        this.game.load.image("platform_2_big", "../../assets/platform_level_3_big.png");

        this.game.load.image("lava", "../../assets/lava.png");
        this.game.load.image("third_level_background", "../../assets/bgforthirdlevel.png");
        this.game.load.image("third_level_background_up", "../../assets/bg3_2.png");
        this.game.load.image("fourth_level_background", "../../assets/bgforfourthlevel.png");
        this.game.load.image("door", "../../assets/door.png");
        this.game.load.image("key", "../../assets/key.png");
        this.game.load.image("firewheel", "../../assets/firewheel.png");
        this.game.load.image("fireenemy", "../../assets/fire_enemy.png");
        this.game.load.spritesheet("wormenemy", "../../assets/worm_enemy.png", 192, 64);



        this.game.load.spritesheet("character", "../../assets/blobster.png", 64, 64);

    },
    /* Function starting the game in the title screen. */
    create: function () {
        "use strict";
        this.game.state.start("titleScreen");
    }
};
