var boot = function () {
    "use strict";
};

boot.prototype = {
    /* global Phaser */
    preload: function () {
        "use strict";
        this.game.load.image("loading", "assets/loading.png");
    },
    create: function () {
        "use strict";
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.setScreenSize();
        this.game.state.start("Preload");
    }
};
