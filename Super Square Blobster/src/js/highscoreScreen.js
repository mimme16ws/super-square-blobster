var heading,
    text,
    x,
    y,
    yHeading,
    yAdded,
    noScoreText,
    levelOneArray,
    levelTwoArray,
    levelThreeArray,
    levelFourArray;

var highscoreScreen = function () {
    "use strict";
    /* global localStorage */
    heading = null;
    text = [];
    x = 80;
    y = 200;
    yHeading = 135;
    yAdded = 50;
    noScoreText = null;
    levelOneArray = [];
    levelTwoArray = [];
    levelThreeArray = [];
    levelFourArray = [];
};

highscoreScreen.prototype = {
    /* Function that initializes the user interface by loading and scaling the images as well as the buttons. */
    create: function () {
        "use strict";
        this.game.add.sprite(0, 0, "bg");
        var blobster = this.game.add.image(380, 150, "highscorePic");
        var backButton = this.game.add.button(0, 0, "button_back", this.goBack, this, 1, 0);
        var levelOneButton = this.game.add.button(184, 0, "button_one", this.openLevelOneScore, this, 1, 0);
        var levelTwoButton = this.game.add.button(338, 0, "button_two", this.openLevelTwoScore, this, 1, 0);
        var levelThreeButton = this.game.add.button(492, 0, "button_three", this.openLevelThreeScore, this, 1, 0);
        var levelFourButton = this.game.add.button(646, 0, "button_four", this.openLevelFourScore, this, 1, 0);
        backButton.scale.setTo(0.7, 0.7);
        blobster.scale.setTo(0.8, 0.8);
        levelOneButton.scale.setTo(0.8, 0.8);
        levelTwoButton.scale.setTo(0.8, 0.8);
        levelThreeButton.scale.setTo(0.8, 0.8);
        levelFourButton.scale.setTo(0.8, 0.8);
        if (localStorage.length !== 0) {
            this.orderHighScoresDecreasingly(1);
            this.changeTableText(1, levelOneArray);
        } else {
            noScoreText = this.game.add.text(x - 40, yHeading, "Noch kein Score vorhanden", {
                fontSize: "28px",
                fill: "#fbb03b",
                stroke: "#000",
                strokeThickness: 8
            });

        }
    }

};

/**
For the level specified in the "levelNumber" parameter, this function takes all the respective highscore entries from the localStorage object and saves them to an array. After that, the single entries are ordered decreasingly.
@param {int} levelNumber - the number of the level that the scores are ordered for.
*/
highscoreScreen.prototype.orderHighScoresDecreasingly = function (levelNumber) {
    "use strict";
    var i = 0,
        p = 0,
        currentEntry;
    switch (levelNumber) {
    case 1:
        for (i; i < localStorage.length; i++) {
            currentEntry = JSON.parse(localStorage.getItem("highScoreEntry" + i));
            if (currentEntry.level === 1) {
                levelOneArray[p] = currentEntry;
                p++;
            }
        }
        // created with the help of http://www.w3schools.com/jsref/jsref_sort.asp
        levelOneArray.sort(function (first, next) {
            return next.accumulatedScore - first.accumulatedScore;
        });
        // return levelOneArray;
        break;
    case 2:
        for (i; i < localStorage.length; i++) {
            currentEntry = JSON.parse(localStorage.getItem("highScoreEntry" + i));
            if (currentEntry.level === 2) {
                levelTwoArray[p] = currentEntry;
                p++;
            }
        }
        // created with the help of http://www.w3schools.com/jsref/jsref_sort.asp
        levelTwoArray.sort(function (first, next) {
            return next.accumulatedScore - first.accumulatedScore;
        });
        // return levelTwoArray;
        break;
    case 3:
        for (i; i < localStorage.length; i++) {
            currentEntry = JSON.parse(localStorage.getItem("highScoreEntry" + i));
            if (currentEntry.level === 3) {
                levelThreeArray[p] = currentEntry;
                p++;
            }
        }
        // created with the help of http://www.w3schools.com/jsref/jsref_sort.asp
        levelThreeArray.sort(function (first, next) {
            return next.accumulatedScore - first.accumulatedScore;
        });
        // return levelThreeArray;
        break;
    case 4:
        for (i; i < localStorage.length; i++) {
            currentEntry = JSON.parse(localStorage.getItem("highScoreEntry" + i));
            if (currentEntry.level === 4) {
                levelFourArray[p] = currentEntry;
                p++;
            }
        }
        // created with the help of http://www.w3schools.com/jsref/jsref_sort.asp
        levelFourArray.sort(function (first, next) {
            return next.accumulatedScore - first.accumulatedScore;
        });
        // return levelFourArray;
        break;
    }
};

/* Function that changes the text of the table to the values asked for by the user.
Depending on the level specified in the paramter "levelNumber" the content of the "levelArray", which is also received as a parameter, is printed on the screen. This, however, counts only for the first five entries of the array.
@param levelNumber - number of the level the score should be shown
@param levelArray - array storing the highscoreObjects containing all the information for the single scores
*/
highscoreScreen.prototype.changeTableText = function (levelNumber, levelArray) {
    "use strict";
    var i;
    this.clearTableText();
    heading = this.game.add.text(x, yHeading, "LEVEL " + levelNumber, {
        fontSize: "28px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
    for (i = 0; i < levelArray.length; i++) {
        text[i] = this.game.add.text(x, y, (i + 1) + ". " + levelArray[i].username + ": " + levelArray[i].accumulatedScore, {
            fontSize: "24px",
            fill: "#fff",
            stroke: "#000",
            strokeThickness: 8
        });
        y += yAdded;
        if (i > 3) {
            y = 200;
            break;
        }
    }
    y = 200;
};

/* Function that clears the text on the screen. This step has to be performed in order to not allow overlay printing. */
highscoreScreen.prototype.clearTableText = function () {
    "use strict";
    var i;
    if (text !== null) {
        for (i = 0; i < text.length; i++) {
            text[i].destroy();
        }
    }
    if (heading !== null) {
        heading.destroy();
    }
};

/* Function that leads the user back to the title screen. */
highscoreScreen.prototype.goBack = function () {
    "use strict";
    this.game.state.start("titleScreen");
};

/* Function that induces the printing of the level 1 highscore on the screen. */
highscoreScreen.prototype.openLevelOneScore = function () {
    "use strict";
    if (noScoreText !== null) {
        noScoreText.destroy();
    }
    this.orderHighScoresDecreasingly(1);
    this.changeTableText(1, levelOneArray);
};

/* Function that induces the printing of the level 2 highscore on the screen. */
highscoreScreen.prototype.openLevelTwoScore = function () {
    "use strict";
    if (noScoreText !== null) {
        noScoreText.destroy();
    }
    this.orderHighScoresDecreasingly(2);
    this.changeTableText(2, levelTwoArray);
};

/* Function that induces the printing of the level 3 highscore on the screen. */
highscoreScreen.prototype.openLevelThreeScore = function () {
    "use strict";
    if (noScoreText !== null) {
        noScoreText.destroy();
    }
    this.orderHighScoresDecreasingly(3);
    this.changeTableText(3, levelThreeArray);
};

/* Function that induces the printing of the level 4 highscore on the screen. */
highscoreScreen.prototype.openLevelFourScore = function () {
    "use strict";
    if (noScoreText !== null) {
        noScoreText.destroy();
    }
    this.orderHighScoresDecreasingly(4);
    this.changeTableText(4, levelFourArray);
};
