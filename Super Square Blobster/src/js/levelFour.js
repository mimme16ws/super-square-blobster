var platforms,
    movablePlatforms,
    player,
    cursors,
    gravity,
    death,
    score,
    stars,
    starIcon,
    timer,
    starsText,
    scoreText,
    timeText,
    collectibles,
    marginScore,
    goal,
    pose,
    died,
    sawblades,
    playerVelocityX,
    playerVelocityY,
    highscoreCounter,
    worldWidth,
    worldHeight,
    backgroundWidth,
    backgroundHeight,
    powerupSlow,
    touchingCollectibles,
    pad1;

var levelFour = function () {
    "use strict";
    platforms = null;
    movablePlatforms = null;
    player = null;
    cursors = null;
    gravity = 400;
    starIcon = null;
    death = null;
    score = 0;
    stars = 0;
    timer = 300;
    starsText = null;
    scoreText = null;
    timeText = null;
    collectibles = null;
    marginScore = 16;
    goal = null;
    pose = false;
    died = false;
    sawblades = null;
    playerVelocityX = 250;
    playerVelocityY = 350;
    highscoreCounter = 0;
    worldWidth = 4800;
    worldHeight = 600;
    backgroundWidth = 800;
    backgroundHeight = 600;
    powerupSlow = false;
    touchingCollectibles = false;
    pad1 = null;
};

levelFour.prototype = {
    /* global Phaser
    global sessionStorage
    global localStorage */
    /* Function that loads all the assets, adds arcade physics, and initializes the groups as well as the UI. */
    create: function () {
        "use strict";
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.world.setBounds(0, 0, worldWidth - 4000, worldHeight + 4200);

        this.createBackground();
        this.initGroups();

        this.addLava(300);
        this.addPlatform(575, 4400, "small", true);
        this.addPlatform(375, 4550, "small", true);
        this.addPlatform(380, 4250, "small", true);
        this.addPlatform(200, 4100, "small", true);
        this.addPlatform(415, 3950, "small", true);
        this.addPlatform(75, 3800, "medium", true);
        this.addPlatform(450, 3300, "small", true);
        this.addPlatform(250, 3150, "small", true);
        this.addPlatform(500, 3000, "medium", true);
        this.addPlatform(450, 2250, "small", true);
        this.addPlatform(250, 2150, "small", true);
        this.addPlatform(450, 2000, "small", true);
        this.addPlatform(0, 1850, "medium", true);

        this.addMovablePlatform(200, 3700, "small");
        this.addMovablePlatform(200, 3550, "small");
        this.addMovablePlatform(200, 3400, "small");
        this.addMovablePlatform(200, 2850, "small");
        this.addMovablePlatform(300, 2700, "small");

        this.addStars(450, 4400, 3, 1, 2);
        this.addStars(415, 4210, 1, 1, 0);
        this.addStars(235, 4060, 1, 1, 0);
        this.addStars(447, 3913, 1, 1, 0);
        this.addStars(75, 3675, 3, 3, 0);
        this.addStars(545, 2805, 4, 4, 0);
        this.addStars(300, 1800, 3, 1, 1);
        this.addStars(334, 2300, 1, 8, 0);

        this.addPowerUp(225, 3730, "slow");
        this.addGround(0, "right");
        this.addGround(500, "left");

        this.addSawblade(490, 4475);
        this.addSawblade(235, 3925);
        this.addSawblade(300, 1910);

        this.addGoal(75, 1210);

        this.initPlayer();
        this.initUI();

        touchingCollectibles = false;
        score = 0;
        stars = 0;
        highscoreCounter = 0;
        this.endPowerUp(1);

        this.game.time.events.add(Phaser.Timer.MINUTE * timer / 60, this.collision, this);
        touchingCollectibles = false;
        this.game.input.gamepad.start();
        pad1 = this.game.input.gamepad.pad1;
    },
    /* Function that is repeatedly called when the game is running. It is responsible for showing animations of the assets as well as the character.
    Moreover, it checks for cheats, updates the UI, and tests whether the character collides with enemies, collectibles, or traps. */
    update: function () {
        "use strict";
        if (!this.game.isPaused) {

            this.game.background.position.x = -(this.game.camera.x * 0.001);

            this.updateCollisions();

            this.checkForCheats();

            this.rotateSawblades();

            this.updatePlayerMovement();

            this.exitLevel();

            this.movePlatform(0, "x", 600, 800, 0.50);
            this.movePlatform(1, "x", 400, 600, 0.50);
            this.movePlatform(2, "x", 600, 800, 0.50);
            this.movePlatform(3, "x", 200, 400, 0.50);
            this.movePlatform(4, "y", 2550, 2000, 0.50);

            timeText.text = this.millisToMinutesAndSeconds(this.game.time.events.duration);
        }
    }
};

/**
Function that adds lava to the game.
@param {int} xPosition - the x coordinate of the lava
*/
levelFour.prototype.addLava = function (xPosition) {
    "use strict";
    var lava = death.create(xPosition, 4717, "lava");
    lava.body.immovable = true;
};

/* Creates the background according to the width and the height defined above. */
levelFour.prototype.createBackground = function () {
    "use strict";
    var i;
    for (i = 0; i < (worldWidth / backgroundHeight).toFixed(0); i++) {
        this.game.background = this.game.add.tileSprite(0, 4200, backgroundWidth, backgroundHeight, "fourth_level_background");
    }
    for (i = 0; i < 7; i++) {
        this.game.background = this.game.add.tileSprite(0, 0 + i * 600, backgroundWidth, backgroundHeight, "third_level_background_up");
    }
    this.game.background.fixedToCamera = false;
};

/* Initializes the user interface, that is to say the texts and icons that inform the user about the time and the collectibles he has collected. */
levelFour.prototype.initUI = function () {
    "use strict";
    scoreText = this.game.add.text(marginScore, marginScore, "000000", {
        fontSize: "24px",
        fill: "#FFF",
        stroke: "#000",
        strokeThickness: 8
    });
    scoreText.fixedToCamera = true;

    starsText = this.game.add.text(350, marginScore, "000", {
        fontSize: "24px",
        fill: "#FFF",
        stroke: "#000",
        strokeThickness: 8
    });
    starsText.fixedToCamera = true;

    starIcon = this.game.add.sprite(410, marginScore, "star");
    starIcon.scale.setTo(0.8, 0.8);
    starIcon.fixedToCamera = true;

    timeText = this.game.add.text(700, marginScore, "3:00", {
        fontSize: "24px",
        fill: "#FFF",
        stroke: "#000",
        strokeThickness: 8
    });
    timeText.fixedToCamera = true;

};

/* Animation that is called when the user has either touched an enemy or a trap. It tests whether the player has collected the shield power up.
After the animation has been played, the level is restarted. */
levelFour.prototype.collision = function () {
    "use strict";
    died = true;
    player.body.collideWorldBounds = false;
    player.body.velocity.x = 0;
    player.body.velocity.y = 0;
    player.body.velocity.y = -400;
    this.game.time.events.add(Phaser.Timer.SECOND * 2.5, this.restartGame, this);
};

/* Restarts the game by resetting all significant variables to their default value. */
levelFour.prototype.restartGame = function () {
    "use strict";
    this.game.world.camera.x = 0;
    score = 0;
    stars = 0;
    pose = false;
    died = false;
    timer = 300;
    this.game.isPaused = false;
    this.game.state.start(this.game.state.current);

};

/**
Function that moves one of the movable platforms. It needs the number of the platform (starting at 0), its direction (either "x" or "y"),
its border (at which it will turn the other way), and its speed.
@param {int} number - number of the platform
@param {String} direction - direction in which the platform moves
@param {int} border - the coordinate value at which the platform stops to move and turns the other way
@param {int} start - the coordinate value where the platform starts
@param {int} speed - the platform's speed
*/
levelFour.prototype.movePlatform = function (number, direction, border, start, speed) {
    "use strict";
    var platform = movablePlatforms.getAt(number);
    switch (direction) {
    case "x":
        if (platform.x < border) {
            platform.body.velocity.x += speed;
        } else {
            platform.body.velocity.x += -speed;
        }
        break;
    case "y":
        if (platform.y < border) {
            platform.body.velocity.y += speed;
        } else {
            platform.body.velocity.y += -speed;
        }
    }

};

/* Function that sets touchingCollectibles back to false which gives the character the ability to jump again, right after having collected a collectible. */
levelFour.prototype.setTouchingCollectiblesBackToFalse = function () {
    "use strict";
    touchingCollectibles = false;
};

/**
Function that is called when the player collects a collectible. It checks whether the collectible is a star, a power up, or the goal. In the former case, it updates the UI respectively and kills the coin. In the case of the goal, this function does nothing. If the collectible is a power up - speed or shield - respective methods are called. By setting touchingCollectibles to true, this function keeps the character from being able to jump when touching the collectibles.
@param {Object} playerCurrent - the character touching the collectible
@param {Object} coin - the collectible touched by the character
*/
levelFour.prototype.collectCoin = function (playerCurrent, coin) {
    "use strict";
    touchingCollectibles = true;
    if (coin.name !== "goal") {
        if (coin.name === "slow") {
            coin.kill();
            this.startPowerUp(1);
            this.game.time.events.add(Phaser.Timer.SECOND * 0.001, this.setTouchingCollectiblesBackToFalse, this);
        } else {
            coin.kill();
            stars++;
            starsText.text = this.pad(stars, 3);
            score += 10;
            scoreText.text = this.pad(score, 6);
            this.game.time.events.add(Phaser.Timer.SECOND * 0.001, this.setTouchingCollectiblesBackToFalse, this);

        }
    }
};

/**
Function that helps with the calculation of the score.
@param {int} num - number of stars or score respectively
@param {int} size - the number of digits being displayed in the UI
@return {String} - the respective UI element
*/
levelFour.prototype.pad = function (num, size) {
    "use strict";
    var s = "000000" + num;
    return s.substr(s.length - size);
};

/**
Function that formats the timer.
@param {int} millis - milliseconds which need to be formated
@return {String} - the respective UI element
*/
levelFour.prototype.millisToMinutesAndSeconds = function (millis) {
    "use strict";
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
};

/* Function that is called when the user finishes the level, it sets explicit varaibles to the needed values, such as pose to true,
which triggers the respective pose animation. It also calls the function which calculates the highscore. */
levelFour.prototype.finishLevel = function () {
    "use strict";
    player.body.velocity.x = 0;
    pose = true;
    goal.frame = 0;
    this.game.time.events.add(Phaser.Timer.SECOND * 5, this.changeLevel, this);
    if (highscoreCounter === 0) {
        this.setHighscore(scoreText.text, timeText.text);
        highscoreCounter++;
    }
};

/** Function that calculates the highscore and saves the user's current score to the browser's localStorage object.
@param {String} scoreCurrent - the user's final score after having finished the level
@param {String} time - the time the user need to complete the level
*/
levelFour.prototype.setHighscore = function (scoreCurrent, time) {
    "use strict";
    var highScoreObject,
        username;
    time = (parseInt(time.substring(0, 1)) * 60) + parseInt(time.substring(2));
    username = sessionStorage.getItem("username");
    highScoreObject = {
        "time": time,
        "username": username,
        "score": scoreCurrent,
        "accumulatedScore": parseInt(score) + time,
        "level": 4
    };
    this.sendScoreToScreen(highScoreObject);
    highScoreObject = JSON.stringify(highScoreObject);
    localStorage.setItem("highScoreEntry" + localStorage.length, highScoreObject);
};

/**
Function that sends information about the user's score to screen. It displays the number of stars the user collected, the time left, and the accumulated score.
@param {Object} highScoreObject - object that contains all the important score information
*/
levelFour.prototype.sendScoreToScreen = function (highScoreObject) {
    "use strict";
    this.game.add.image(this.game.camera.x + 200, this.game.camera.y + 100, "highScoreLevelEnd");
    this.game.add.text(this.game.camera.x + 350, this.game.camera.y + 176, (String(highScoreObject.score)).substring(3, 5), {
        fontSize: "48px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
    this.game.add.text(this.game.camera.x + 350, this.game.camera.y + 246, highScoreObject.time, {
        fontSize: "48px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
    this.game.add.text(this.game.camera.x + 350, this.game.camera.y + 316, highScoreObject.accumulatedScore, {
        fontSize: "48px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
};

/**
Function that is called when the level is finished. It sets important variables back to their default values and starts the next level.
*/
levelFour.prototype.changeLevel = function () {
    "use strict";
    pose = false;
    this.game.state.start("levelSelect");
};

/**
Function that adds a spinning sawblade to the level.
@param {int} xPosition - the x coordinate of the sawblade
@param {int} yPosition - the y coordinate of the sawblade
*/
levelFour.prototype.addSawblade = function (xPosition, yPosition) {
    "use strict";
    var sawblade;
    sawblade = sawblades.create(xPosition, yPosition, "sawblade");
    sawblade.animations.add("rotate", [0, 1, 2, 3, 4, 5, 6], 30, true);
    sawblade.body.immovable = true;
    sawblade.body.width = 64;
    sawblade.body.height = 64;
    sawblade.body.x = 3;
    sawblade.body.y = 3;
};

/**
Function that adds a platform to the level.
@param {int} xPosition - the x coordinate of the platform
@param {int} yPosition - the y coordinate of the platform
@param {String} size - the size of the platform: "small", "medium", or "big"
@param {boolean} immovable - value that defines whether the platform is immovable or not
*/
levelFour.prototype.addPlatform = function (xPosition, yPosition, size, immovable) {
    "use strict";
    switch (size) {
    case "small":
        var ledge1 = platforms.create(xPosition, yPosition, "platform_2_small");
        ledge1.body.setSize(100, 25, 0, 5);
        ledge1.body.immovable = immovable;
        break;
    case "medium":
        var ledge2 = platforms.create(xPosition, yPosition, "platform_2_medium");
        ledge2.body.setSize(230, 25, 0, 5);
        ledge2.body.immovable = immovable;
        break;
    case "big":
        var ledge3 = platforms.create(xPosition, yPosition, "platform_2_big");
        ledge3.body.setSize(360, 25, 0, 5);
        ledge3.body.immovable = immovable;
        break;
    }
};

/**
Function that adds a movable platform to the level. Just as in the function for the normal platforms,
this function also needs coordinates and a value for the size of the platform to be created.
HOWEVER, the platforms created in this function are different because they ae stored in a different array and need to be called in update() with the movePlatform()-method.
@param {int} xPosition - the x coordinate of the movable platform
@param {int} yPosition - the y coordinate of the movable platform
@param {String} size - the size of the  movable platform: "small", "medium", or "big"
*/
levelFour.prototype.addMovablePlatform = function (xPosition, yPosition, size) {
    "use strict";
    switch (size) {
    case "small":
        var ledge1 = movablePlatforms.create(xPosition, yPosition, "platform_2_small");
        ledge1.body.setSize(100, 25, 0, 5);
        ledge1.body.immovable = true;
        break;
    case "medium":
        var ledge2 = movablePlatforms.create(xPosition, yPosition, "platform_2_medium");
        ledge2.body.setSize(230, 25, 0, 5);
        ledge2.body.immovable = true;
        break;
    case "big":
        var ledge3 = movablePlatforms.create(xPosition, yPosition, "platform_2_big");
        ledge3.body.setSize(360, 25, 0, 5);
        ledge3.body.immovable = true;
        break;
    }
};

/**
Function that adds a ground platform to the level. It needs an x coordinate and the corner which is supposed to be on the outside.
For instance, it only needs the right one at the start of the level since the left side is never shown on the screen.
@param {int} xPosition - the x coordinate of the ground platform
@param {String} corners - offers information on which side of the ground platform has a black outline
*/
levelFour.prototype.addGround = function (xPosition, corners) {
    "use strict";
    switch (corners) {
    case "left":
        var ground1 = platforms.create(xPosition, worldHeight - 104 + 4200, "ground_2_left");
        ground1.body.immovable = true;
        break;
    case "middle":
        var ground2 = platforms.create(xPosition, worldHeight - 104 + 4200, "ground_2");
        ground2.body.immovable = true;
        break;
    case "right":
        var ground3 = platforms.create(xPosition, worldHeight - 104 + 4200, "ground_2_right");
        ground3.body.immovable = true;
        break;
    case "single":
        var ground4 = platforms.create(xPosition, worldHeight - 104 + 4200, "ground_2_all");
        ground4.body.immovable = true;
        break;
    }
};

/**
Function that adds a cluster of stars. It needs coordinates, the number of rows and columns, as well as a mode.
mode:    0 -> not diagonal
         1 -> diagonal ascending
         2 -> diagonal descending
@param {int} xPosition - the x coordinate of the star cluster
@param {int} yPosition - the y coordinate of the star cluster
@param {int} rows - number of rows
@param {int} cols - number of columns
@param {int} diagonal - information on the mode (see above)
*/
levelFour.prototype.addStars = function (xPosition, yPosition, rows, cols, diagonal) {
    "use strict";
    var i,
        j;
    if (diagonal === 1) { //ascending
        for (i = 0; i < rows; i++) {
            collectibles.create(xPosition + i * 38, yPosition + i * 38, "star");
        }
    } else if (diagonal === 2) {
        for (i = 0; i < rows; i++) {
            collectibles.create(xPosition + i * 38, yPosition - i * 38, "star");
        }
    } else {
        for (i = 0; i < cols; i++) {
            for (j = 0; j < rows; j++) {
                collectibles.create(xPosition + j * 38, yPosition + i * 38, "star");
            }
        }
    }
};

/**
Function that adds a goal to the level.
@param {int} xPosition - the x coordinate of the goal
@param {int} yPosition - the y coordinate of the goal
*/
levelFour.prototype.addGoal = function (xPosition, yPosition) {
    "use strict";
    goal = collectibles.create(xPosition, yPosition - 22 + 600, "goalnew");
    goal.frame = 1;
    goal.name = "goal";
};

/* Function that initiates the player sprite, gives him a body, adds animations, etc. */
levelFour.prototype.initPlayer = function () {
    "use strict";
    player = this.game.add.sprite(32, 4600, "character");

    this.game.physics.enable(player, Phaser.Physics.ARCADE);
    player.body.setSize(50, 64, 5, 0);
    player.body.gravity.y = gravity;
    player.body.collideWorldBounds = true;

    player.animations.add("left", [1, 2, 3, 4], 11, true);
    player.animations.add("right", [7, 8, 9, 10], 11, true);
    player.animations.add("slow_left", [44, 45, 46, 47], 11, true);
    player.animations.add("slow_right", [50, 51, 52, 53], 11, true);

    this.game.camera.follow(player);
};

/* Function that initiates the different object groups. */
levelFour.prototype.initGroups = function () {
    "use strict";
    death = this.game.add.group();
    death.enableBody = true;
    platforms = this.game.add.group();
    platforms.enableBody = true;
    sawblades = this.game.add.group();
    sawblades.enableBody = true;
    movablePlatforms = this.game.add.group();
    movablePlatforms.enableBody = true;
    collectibles = this.game.add.group();
    collectibles.enableBody = true;
};

/* Checks for collisions of the player with objects in the game. */
levelFour.prototype.updateCollisions = function () {
    "use strict";
    if (!died) {
        this.game.physics.arcade.collide(player, platforms);
        this.game.physics.arcade.collide(player, movablePlatforms);
        this.game.physics.arcade.overlap(player, goal, this.finishLevel, null, this);
        this.game.physics.arcade.collide(player, death, this.collision, null, this);
        this.game.physics.arcade.overlap(player, collectibles, this.collectCoin, null, this);
        this.game.physics.arcade.collide(player, sawblades, this.collision, null, this);
        //this.game.physics.arcade.collide(player, enemies, this.collision, null, this);
    }
};

/* Function that updates the player movement. It is called in the update() function and checks for all kinds of different states, and shows the respective animations. */
levelFour.prototype.updatePlayerMovement = function () {
    "use strict";
    if (!pose && !died) {
        cursors = this.game.input.keyboard.createCursorKeys();
        player.body.velocity.x = 0;
        if (cursors.left.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1) {
            player.body.velocity.x = -playerVelocityX;
            if (powerupSlow === true) {
                player.animations.play("slow_left");
            } else {
                player.animations.play("left");
            }
        } else if (cursors.right.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1) {
            player.body.velocity.x = playerVelocityX;
            if (powerupSlow === true) {
                player.animations.play("slow_right");
            } else {
                player.animations.play("right");
            }
        } else {
            player.animations.stop();
            if (powerupSlow) {
                player.frame = 48;
            } else {
                player.frame = 5;
            }
        }
        if (cursors.up.isDown && player.body.touching.down && !touchingCollectibles || pad1.justPressed(Phaser.Gamepad.XBOX360_A) && player.body.touching.down && !touchingCollectibles) {
            player.body.velocity.y = -playerVelocityY;
        }
        if (player.body.velocity.y < 0) {
            if (cursors.left.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1) {
                player.animations.stop();
                if (powerupSlow) {
                    player.frame = 43;
                } else {
                    player.frame = 0;
                }
            }
            if (cursors.right.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1) {
                player.animations.stop();
                if (powerupSlow) {
                    player.frame = 54;
                } else {
                    player.frame = 11;
                }
            }
        }
    } else if (pose && powerupSlow) {
        player.frame = 49;
    } else if (pose) {
        player.frame = 6;
    } else if (died && player.body.velocity.y < 0) {
        if (powerupSlow) {
            player.frame = 55;
        } else {
            player.frame = 12;
        }
    } else if (powerupSlow) {
        player.frame = 56;
    } else {
        player.frame = 13;
    }
};

/* Function that checks whether the user entered a cheat code. */
levelFour.prototype.checkForCheats = function () {
    "use strict";
    if (this.game.input.keyboard.isDown(Phaser.Keyboard.G)) {
        playerVelocityX = 600;
        playerVelocityY = 600;
    } else if (this.game.input.keyboard.isDown(Phaser.Keyboard.K)) {
        playerVelocityX = 250;
        playerVelocityY = 350;
    }
};

/* Function that rotates the sawblades, that is to say that loads the respective animations. */
levelFour.prototype.rotateSawblades = function () {
    "use strict";
    for (var i = 0; i < sawblades.children.length; i++) {
        sawblades.getChildAt(i).animations.play("rotate");
    }
};

/* Function that is triggered when the user hits the ESC key, which then makes the game return to the title screen. */
levelFour.prototype.exitLevel = function () {
    "use strict";
    if (this.game.input.keyboard.isDown(Phaser.Keyboard.ESC) || pad1.justPressed(Phaser.Gamepad.XBOX360_START)) {
        this.game.state.start("titleScreen");
    }
};

/**
Function that adds a power up to the game.
@param {int} xPosition - the x coordinate of the power up
@param {int} yPosition - the y coordinate of the power up
@param {String} kindOfPowerUp - specifies the kind of power up, that is to say "slow"
*/
levelFour.prototype.addPowerUp = function (xPosition, yPosition, kindOfPowerUp) {
    "use strict";
    var powerUp = collectibles.create(xPosition, yPosition, kindOfPowerUp);
    powerUp.name = kindOfPowerUp;
};

/**
Function that starts the power up.
@param {int} num - specifies the number of the power up (1 = slow power up) -> identifier
*/
levelFour.prototype.startPowerUp = function (num) {
    "use strict";
    if (num === 1) {
        playerVelocityX = 150;
        powerupSlow = true;
        this.game.time.events.add(Phaser.Timer.SECOND * 5, this.endPowerUp, this, num);
    }
};

/**
Function that ends the power up.
@param {int} num - specifies the number of the power up (1 = slow) -> identifier
*/
levelFour.prototype.endPowerUp = function (num) {
    "use strict";
    if (num === 1) {
        playerVelocityX = 250;
        powerupSlow = false;
    }
};

/* Function that is triggered when the user hits the ESC key, which then makes the game return to the title screen. */
levelFour.prototype.exitLevel = function () {
    "use strict";
    if (this.game.input.keyboard.isDown(Phaser.Keyboard.ESC)) {
        this.game.state.start("titleScreen");
    }
};
