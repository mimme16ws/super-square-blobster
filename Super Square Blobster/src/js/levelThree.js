var platforms,
    movablePlatforms,
    player,
    cursors,
    gravity,
    death,
    score,
    stars,
    starsText,
    scoreText,
    timeText,
    collectibles,
    marginScore,
    goal,
    spikes,
    pose,
    died,
    sawblades,
    starIcon,
    playerVelocityX,
    playerVelocityY,
    worldWidth,
    worldHeight,
    shield,
    hitDeath,
    backgroundWidth,
    backgroundHeight,
    enemy,
    enemies,
    touchingCollectibles,
    keycheck,
    keyIcon,
    highscoreCounter = 0,
    pad1;

var levelThree = function () {
    "use strict";
    platforms = null;
    movablePlatforms = null;
    player = null;
    cursors = null;
    gravity = 600;
    death = null;
    score = 0;
    stars = 0;
    starIcon = null;
    starsText = null;
    scoreText = null;
    timeText = null;
    collectibles = null;
    marginScore = 16;
    goal = null;
    spikes = null;
    pose = false;
    died = false;
    sawblades = null;
    playerVelocityX = 250;
    playerVelocityY = 350;
    worldWidth = 4800;
    worldHeight = 600;
    shield = false;
    hitDeath = false;
    backgroundWidth = 800;
    backgroundHeight = 600;
    enemy = null;
    enemies = null;
    touchingCollectibles = false;
    keycheck = false;
    keyIcon = null;
    highscoreCounter = 0;
    pad1 = null;
};

/* Since the level's height is 1200, that is to say twice the height of the two levels before, 600 px is added to each and every y coordinate
which is handed over in all the methods. */


levelThree.prototype = {
    /* global Phaser
    global sessionStorage
    global localStorage */
    /* Function that loads all the assets, adds arcade physics, and initializes the groups as well as the UI. */
    create: function () {
        "use strict";
        var i;
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.world.setBounds(0, 0, worldWidth + 3200, worldHeight + worldHeight);
        keycheck = false;
        this.createBackground();
        this.initGroups();

        for (i = 0; i < 32; i++) {
            this.addLava(i * 305);
        }
        this.addPlatform(50, 100, "small", true);
        this.addPlatform(350, -85, "small", true);
        this.addPlatform(450, 350, "big", true);
        this.addPlatform(600, 30, "small", true);
        this.addPlatform(900, 80, "small", true);
        this.addPlatform(1400, 350, "big", true);
        this.addPlatform(1150, 220, "small", true);
        this.addPlatform(3200, 350, "medium", false);
        this.addPlatform(3500, 250, "small", true);
        this.addPlatform(3700, 125, "small", false);
        this.addPlatform(3900, 100, "medium", false);
        this.addPlatform(4300, 0, "small", false);
        this.addPlatform(5100, 400, "medium", false);

        this.addDoor(2790, 312, "door1");
        this.addDoor(5730, 312, "door2");

        this.addStars(84, -30, 1, 3, 0);
        this.addStars(590, 200, 2, 2, 0);
        this.addStars(820, 190, 3, 0, 1);
        this.addStars(1184, 140, 1, 2, 0);
        this.addStars(1500, 205, 2, 1, 2);
        this.addStars(1585, 167, 2, 1, 1);
        this.addStars(1960, 440, 3, 1, 0);
        this.addStars(2300, 395, 2, 1, 2);
        this.addStars(2385, 357, 2, 1, 1);
        this.addStars(3400, 280, 2, 1, 2);
        this.addStars(3580, 135, 2, 1, 2);
        this.addStars(3830, 60, 1, 1, 0);
        this.addStars(3981, 10, 2, 2, 0);
        this.addStars(4334, 150, 1, 4, 0);
        this.addStars(4900, 400, 3, 0, 2);
        this.addStars(5419, 340, 1, 1, 0);
        this.addStars(6350, 300, 4, 1, 2);
        this.addStars(6910, 360, 3, 1, 1);

        this.addPowerUp(926, 20, "shield");

        this.addMovablePlatform(6050, 360, "small");
        this.addMovablePlatform(6800, 280, "small");

        this.addGround(0, "right");
        this.addGround(900, "single");
        this.addGround(1950, "left");
        this.addGround(2300, "middle");
        this.addGround(2645, "right");
        this.addGround(4700, "single");
        this.addGround(5600, "single");
        this.addGround(7000, "single");

        this.addASpikePit(4950, 408);

        this.addEnemy(1300, 200, "fireenemy");
        this.addEnemy(700, 286, "ninjaenemy");
        this.addEnemy(2500, 432, "wormenemy");
        this.addEnemy(3050, 200, "fireenemy");

        this.initPlayer();
        this.initUI();

        this.addSawblade(750, 50);
        this.addSawblade(1545, 250);
        this.addSawblade(5400, 400);

        this.addKey(375, -120);
        this.addKey(5200, 450);

        this.addGoal(7230, 460);

        this.game.time.events.add(Phaser.Timer.MINUTE * 3.5, this.collision, this);
        touchingCollectibles = false;
        score = 0;
        stars = 0;
        highscoreCounter = 0;
        shield = false;
        hitDeath = false;
        this.game.input.gamepad.start();
        pad1 = this.game.input.gamepad.pad1;
    },

    /* Function that is repeatedly called when the game is running. It is responsible for showing animations of the assets as well as the character. Moreover, it checks for cheats, updates the UI, and tests whether the character collides with enemies, collectibles, or traps. */
    update: function () {
        "use strict";
        if (!this.game.isPaused) {

            this.game.background.position.x = -(this.game.camera.x * 0.001);
            this.updateCollisions();
            this.checkForCheats();
            this.rotateSawblades();
            this.updatePlayerMovement();
            this.exitLevel();

            this.moveEnemies(0, 1000, 5, "y");
            this.moveEnemies(1, 600, 2, "x");
            this.moveEnemies(2, 2300, 1, "x");
            this.moveEnemies(3, 1000, 5, "y");

            this.movePlatform(0, "x", 6140, 6050, 0.75);
            this.movePlatform(1, "x", 6710, 6800, 0.75);

            timeText.text = this.millisToMinutesAndSeconds(this.game.time.events.duration);
        }
    }

};

/**
Adds a key to the game. The key is needed to open the big doors in the level.
@param {int} xPosition - the key's x coordniate
@param {int} yPosition - the key's y coordniate
*/
levelThree.prototype.addKey = function (xPosition, yPosition) {
    "use strict";
    var key = collectibles.create(xPosition, yPosition + worldHeight, "key");
    key.name = "key";
};

/**
Adds a spike pit to the game.
@param {int} xPosition - the spike pit's x coordniate
@param {int} yPosition - the spike pit's y coordniate
*/
levelThree.prototype.addASpikePit = function (xPosition, yPosition) {
    "use strict";
    spikes = death.create(xPosition, yPosition + worldHeight, "spikes");
    spikes.body.immovable = true;
};

/**
Function that adds a power up to the game.
@param {int} xPosition - the x coordinate of the power up
@param {int} yPosition - the y coordinate of the power up
@param {String} kindOfPowerUp - specifies the kind of power up, that is to say "shield"
*/
levelThree.prototype.addPowerUp = function (xPosition, yPosition, kindOfPowerUp) {
    "use strict";
    var powerUp = collectibles.create(xPosition, yPosition + worldHeight, kindOfPowerUp);
    powerUp.name = kindOfPowerUp;
};

/**
Function that adds a door to the game.
@param {int} xPosition - the x coordinate of the door
@param {int} yPosition - the y coordinate of the door
@param {String} doorName - specifies the door's name, works as an identifier
*/
levelThree.prototype.addDoor = function (xPosition, yPosition, doorName) {
    "use strict";
    var door = platforms.create(xPosition, yPosition + worldHeight, "door");
    door.body.immovable = true;
    door.name = doorName;
};

/**
Function that adds lava to the game.
@param {int} xPosition - the x coordinate of the lava
*/
levelThree.prototype.addLava = function (xPosition) {
    "use strict";
    var lava = death.create(xPosition, 1117, "lava");
    lava.body.immovable = true;
};

/* Creates the background according to the width and the height defined above. Another background image is added since the world has a height of 1200 px. */
levelThree.prototype.createBackground = function () {
    "use strict";
    var i;
    for (i = 0; i < ((worldWidth + 4800) / backgroundHeight).toFixed(0); i++) {
        this.game.background = this.game.add.tileSprite(0, worldHeight, backgroundWidth + i * backgroundWidth, backgroundHeight, "third_level_background");
    }
    for (i = 0; i < ((worldWidth + 4800) / backgroundHeight).toFixed(0); i++) {
        this.game.background = this.game.add.tileSprite(0, 0, backgroundWidth + i * backgroundWidth, backgroundHeight, "third_level_background_up");
    }
    this.game.background.fixedToCamera = false;
};

/* Initializes the user interface, that is to say the texts and icons that inform the user about the time and the collectibles he has collected. */
levelThree.prototype.initUI = function () {
    "use strict";
    scoreText = this.game.add.text(marginScore, marginScore, "000000", {
        fontSize: "24px",
        fill: "#FFF",
        stroke: "#000",
        strokeThickness: 8
    });
    scoreText.fixedToCamera = true;

    starsText = this.game.add.text(350, marginScore, "000", {
        fontSize: "24px",
        fill: "#FFF",
        stroke: "#000",
        strokeThickness: 8
    });
    starsText.fixedToCamera = true;

    keyIcon = this.game.add.sprite(300, marginScore + 10, "key");
    keyIcon.fixedToCamera = true;
    keyIcon.scale.setTo(0.6, 0.6);
    keyIcon.visible = false;

    starIcon = this.game.add.sprite(410, marginScore + 5, "star");
    starIcon.scale.setTo(0.8, 0.8);
    starIcon.fixedToCamera = true;

    timeText = this.game.add.text(700, marginScore, "3:00", {
        fontSize: "24px",
        fill: "#FFF",
        stroke: "#000",
        strokeThickness: 8
    });
    timeText.fixedToCamera = true;
};

/* Animation that is called when the user has either touched an enemy or a trap. It tests whether the player has collected the shield power up.
After the animation has been played, the level is restarted. */
levelThree.prototype.collision = function () {
    "use strict";
    if (!shield) {
        died = true;
        player.body.collideWorldBounds = false;
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        player.body.velocity.y = -400;
        this.game.time.events.add(Phaser.Timer.SECOND * 2.5, this.restartGame, this);
    } else {
        hitDeath = true;
        this.game.time.events.add(Phaser.Timer.SECOND, this.setShieldBackToFalse, this);
    }
};

/* Sets the shield boolean value back to false. This function is called when the user, having collected the hsiled beforehand, has touched either an enemy or a trap. */
levelThree.prototype.setShieldBackToFalse = function () {
    "use strict";
    shield = false;
    hitDeath = false;
};


/* Restarts the game by resetting all significant variables to their default value. */
levelThree.prototype.restartGame = function () {
    "use strict";
    this.game.world.camera.x = 0;
    score = 0;
    stars = 0;
    pose = false;
    died = false;
    this.game.isPaused = false;
    this.game.state.start(this.game.state.current);
};

/**
Adds an enemy to the game including left and right walking animations.
@param {int} xPosition - the enemy's x coordniate
@param {int} yPosition - the enemy's y coordniate
@param {String} type - they type of the created enemy
*/
levelThree.prototype.addEnemy = function (xPosition, yPosition, type) {
    "use strict";
    if (type === "fireenemy") {
        enemy = enemies.create(xPosition, yPosition + worldHeight, "fireenemy");
        enemy.body.immovable = true;
    } else if (type === "wormenemy") {
        enemy = enemies.create(xPosition, yPosition + worldHeight, "wormenemy");
        enemy.body.immovable = true;
        enemy.animations.add("left", [0, 1], 5, true);
        enemy.animations.add("right", [2, 3], 5, true);
    } else if (type === "ninjaenemy") {
        enemy = enemies.create(xPosition, yPosition + worldHeight, "enemy");
        enemy.body.immovable = true;
        enemy.animations.add("left", [0, 1], 5, true);
        enemy.animations.add("right", [3, 4], 5, true);
    }
};

/**
Function that moves one of the movable platforms. It needs the number of the platform (starting at 0), its direction (either "x" or "y"),
its border (at which it will turn the other way), and its speed.
@param {int} number - number of the platform
@param {String} direction - direction in which the platform moves
@param {int} border - the coordinate value at which the platform stops to move and turns the other way
@param {int} start - the coordinate value where the platform starts
@param {int} speed - the platform's speed
*/
levelThree.prototype.movePlatform = function (number, direction, border, start, speed) {
    "use strict";
    var platform = movablePlatforms.getAt(number);
    switch (direction) {
    case "x":
        if (platform.x < border) {
            platform.body.velocity.x += speed;
        } else {
            platform.body.velocity.x += -speed;
        }
        break;
    case "y":
        if (platform.y < border) {
            platform.body.velocity.y += speed;
        } else {
            platform.body.velocity.y += -speed;
        }
    }
};

/**
Function which makes enemies move.
@param {int} number - number of the enemy
@param {int} border - the coordinate value at which the enemy stops to move and turns the other way
@param {int} speed - the enemy's speed
@param {String} dir - direction of the movement; either horizontal ("x") or vertical ("y")
*/
levelThree.prototype.moveEnemies = function (number, border, speed, dir) {
    "use strict";
    var enemyCurrent = enemies.getAt(number);
    switch (dir) {
    case "x":
        if (enemyCurrent.x < border) {
            enemyCurrent.body.velocity.x += speed;
        } else {
            enemyCurrent.body.velocity.x += -speed;
        }
        if (enemyCurrent.body.velocity.x < 0) {
            enemyCurrent.animations.play("left");
        } else {
            enemyCurrent.animations.play("right");
        }
        break;
    case "y":
        if (enemyCurrent.y < border) {
            enemyCurrent.body.velocity.y += speed;
        } else {
            enemyCurrent.body.velocity.y += -speed;
        }
        break;
    }
};

/** Function that calculates the highscore and saves the user's current score to the browser's localStorage object.
@param {String} scoreCurrent - the user's final score after having finished the level
@param {String} time - the time the user need to complete the level
*/
levelThree.prototype.setHighscore = function (scoreCurrent, time) {
    "use strict";
    var highScoreObject,
        username;
    time = (parseInt(time.substring(0, 1)) * 60) + parseInt(time.substring(2));
    username = sessionStorage.getItem("username");
    highScoreObject = {
        "time": time,
        "username": username,
        "score": scoreCurrent,
        "accumulatedScore": parseInt(score) + time,
        "level": 3
    };
    this.sendScoreToScreen(highScoreObject);
    highScoreObject = JSON.stringify(highScoreObject);
    localStorage.setItem("highScoreEntry" + localStorage.length, highScoreObject);
};

/**
Function that sends information about the user's score to screen. It displays the number of stars the user collected, the time left, and the accumulated score.
@param {Object} highScoreObject - object that contains all the important score information
*/
levelThree.prototype.sendScoreToScreen = function (highScoreObject) {
    "use strict";
    this.game.add.image(this.game.camera.x + 200, 100 + worldHeight, "highScoreLevelEnd");
    this.game.add.text(this.game.camera.x + 350, 176 + worldHeight, (String(highScoreObject.score)).substring(3, 5), {
        fontSize: "48px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
    this.game.add.text(this.game.camera.x + 350, 246 + worldHeight, highScoreObject.time, {
        fontSize: "48px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
    this.game.add.text(this.game.camera.x + 350, 316 + worldHeight, highScoreObject.accumulatedScore, {
        fontSize: "48px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
};

levelThree.prototype.setTouchingCollectiblesBackToFalse = function () {
    "use strict";
    touchingCollectibles = false;
};

/**
Function that is called when the player collects a collectible. It checks whether the collectible is a star, a power up, or the goal. In the former case, it updates the UI respectively and kills the coin. In the case of the goal, this function does nothing. If the collectible is a power up - speed or shield - respective methods are called. By setting touchingCollectibles to true, this function keeps the character from being able to jump when touching the collectibles.
@param {Object} playerCurrent - the character touching the collectible
@param {Object} coin - the collectible touched by the character
*/
levelThree.prototype.collectCoin = function (playerCurrent, coin) {
    "use strict";
    touchingCollectibles = true;
    if (coin.name !== "goal") {
        if (coin.name === "key") {
            coin.kill();
            keycheck = true;
            keyIcon.visible = true;
            this.game.time.events.add(Phaser.Timer.SECOND * 0.001, this.setTouchingCollectiblesBackToFalse, this);
        } else if (coin.name === "shield") {
            coin.kill();
            shield = true;
            this.game.time.events.add(Phaser.Timer.SECOND * 0.001, this.setTouchingCollectiblesBackToFalse, this);
        } else {
            coin.kill();
            stars++;
            starsText.text = this.pad(stars, 3);
            score += 10;
            scoreText.text = this.pad(score, 6);
            this.game.time.events.add(Phaser.Timer.SECOND * 0.001, this.setTouchingCollectiblesBackToFalse, this);
        }
    }
};

/**
Function that helps with the calculation of the score.
@param {int} num - number of stars or score respectively
@param {int} size - the number of digits being displayed in the UI
@return {String} - the respective UI element
*/
levelThree.prototype.pad = function (num, size) {
    "use strict";
    var s = "000000" + num;
    return s.substr(s.length - size);
};

/**
Function that formats the timer.
@param {int} millis - milliseconds which need to be formated
@return {String} - the respective UI element
*/
levelThree.prototype.millisToMinutesAndSeconds = function (millis) {
    "use strict";
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
};

/* Function that is called when the user finishes the level, it sets explicit varaibles to the needed values, such as pose to true,
which triggers the respective pose animation. It also calls the function which calculates the highscore. */
levelThree.prototype.finishLevel = function () {
    "use strict";
    player.body.velocity.x = 0;
    pose = true;
    goal.frame = 0;
    this.game.time.events.add(Phaser.Timer.SECOND * 5, this.changeLevel, this);
    if (highscoreCounter === 0) {
        this.setHighscore(scoreText.text, timeText.text);
        highscoreCounter++;
    }
};

/**
Function that is called when the level is finished. It sets important variables back to their default values and starts the next level.
*/
levelThree.prototype.changeLevel = function () {
    "use strict";
    pose = false;
    this.game.state.start("levelFour");
};


/**
Function that adds a spinning sawblade to the level.
@param {int} xPosition - the x coordinate of the sawblade
@param {int} yPosition - the y coordinate of the sawblade
*/
levelThree.prototype.addSawblade = function (xPosition, yPosition) {
    "use strict";
    var sawblade;
    sawblade = sawblades.create(xPosition, yPosition + worldHeight, "sawblade");
    sawblade.animations.add("rotate", [0, 1, 2, 3, 4, 5, 6], 30, true);
    sawblade.body.immovable = true;
    sawblade.body.width = 64;
    sawblade.body.height = 64;
    sawblade.body.x = 3;
    sawblade.body.y = 3;
};

/**
Function that adds a platform to the level.
@param {int} xPosition - the x coordinate of the platform
@param {int} yPosition - the y coordinate of the platform
@param {String} size - the size of the platform: "small", "medium", or "big"
@param {boolean} immovable - value that defines whether the platform is immovable or not
*/
levelThree.prototype.addPlatform = function (xPosition, yPosition, size, immovable) {
    "use strict";
    switch (size) {
    case "small":
        var ledge1 = platforms.create(xPosition, yPosition + worldHeight, "platform_2_small");
        ledge1.body.setSize(100, 25, 0, 5);
        ledge1.body.immovable = immovable;
        break;
    case "medium":
        var ledge2 = platforms.create(xPosition, yPosition + worldHeight, "platform_2_medium");
        ledge2.body.setSize(230, 25, 0, 5);
        ledge2.body.immovable = immovable;
        break;
    case "big":
        var ledge3 = platforms.create(xPosition, yPosition + worldHeight, "platform_2_big");
        ledge3.body.setSize(360, 25, 0, 5);
        ledge3.body.immovable = immovable;
        break;
    }
};

/**
Function that adds a movable platform to the level. Just as in the function for the normal platforms,
this function also needs coordinates and a value for the size of the platform to be created.
HOWEVER, the platforms created in this function are different because they ae stored in a different array and need to be called in update() with the movePlatform()-method.
@param {int} xPosition - the x coordinate of the movable platform
@param {int} yPosition - the y coordinate of the movable platform
@param {String} size - the size of the  movable platform: "small", "medium", or "big"
*/
levelThree.prototype.addMovablePlatform = function (xPosition, yPosition, size) {
    "use strict";
    switch (size) {
    case "small":
        var ledge1 = movablePlatforms.create(xPosition, yPosition + worldHeight, "platform_2_small");
        ledge1.body.immovable = true;
        break;
    case "medium":
        var ledge2 = movablePlatforms.create(xPosition, yPosition + worldHeight, "platform_2_medium");
        ledge2.body.immovable = true;
        break;
    case "big":
        var ledge3 = movablePlatforms.create(xPosition, yPosition + worldHeight, "platform_2_big");
        ledge3.body.immovable = true;
        break;
    }
};

/**
Function that adds a ground platform to the level. It needs an x coordinate and the corner which is supposed to be on the outside.
For instance, it only needs the right one at the start of the level since the left side is never shown on the screen.
@param {int} xPosition - the x coordinate of the ground platform
@param {String} corners - offers information on which side of the ground platform has a black outline
*/
levelThree.prototype.addGround = function (xPosition, corners) {
    "use strict";
    switch (corners) {
    case "left":
        var ground1 = platforms.create(xPosition, worldHeight - 104 + worldHeight, "ground_2_left");
        ground1.body.immovable = true;
        break;
    case "middle":
        var ground2 = platforms.create(xPosition, worldHeight - 104 + worldHeight, "ground_2");
        ground2.body.immovable = true;
        break;
    case "right":
        var ground3 = platforms.create(xPosition, worldHeight - 104 + worldHeight, "ground_2_right");
        ground3.body.immovable = true;
        break;
    case "single":
        var ground4 = platforms.create(xPosition, worldHeight - 104 + worldHeight, "ground_2_all");
        ground4.body.immovable = true;
        break;
    }
};

/**
Function that adds a cluster of stars. It needs coordinates, the number of rows and columns, as well as a mode.
mode:    0 -> not diagonal
         1 -> diagonal ascending
         2 -> diagonal descending
@param {int} xPosition - the x coordinate of the star cluster
@param {int} yPosition - the y coordinate of the star cluster
@param {int} rows - number of rows
@param {int} cols - number of columns
@param {int} diagonal - information on the mode (see above)
*/
levelThree.prototype.addStars = function (xPosition, yPosition, rows, cols, diagonal) {
    "use strict";
    var j,
        i;
    if (diagonal === 1) { //ascending
        for (i = 0; i < rows; i++) {
            collectibles.create(xPosition + i * 38, yPosition + i * 38 + worldHeight, "star");
        }
    } else if (diagonal === 2) {
        for (i = 0; i < rows; i++) {
            collectibles.create(xPosition + i * 38, yPosition - i * 38 + worldHeight, "star");
        }
    } else {
        for (i = 0; i < cols; i++) {
            for (j = 0; j < rows; j++) {
                collectibles.create(xPosition + j * 38, yPosition + i * 38 + worldHeight, "star");
            }
        }
    }
};

/**
Function that adds a goal to the level.
@param {int} xPosition - the x coordinate of the goal
@param {int} yPosition - the y coordinate of the goal
*/
levelThree.prototype.addGoal = function (xPosition, yPosition) {
    "use strict";
    goal = collectibles.create(xPosition, yPosition - 22 + worldHeight, "goalnew");
    goal.frame = 1;
    goal.name = "goal";
};

/* Function that initiates the player sprite, gives him a body, adds animations, etc. */
levelThree.prototype.initPlayer = function () {
    "use strict";
    player = this.game.add.sprite(32, worldHeight - 200 + worldHeight, "character");
    this.game.physics.enable(player, Phaser.Physics.ARCADE);
    player.body.setSize(50, 64, 5, 0);
    player.body.gravity.y = gravity;
    player.body.collideWorldBounds = true;
    player.animations.add("left", [1, 2, 3, 4], 11, true);
    player.animations.add("right", [7, 8, 9, 10], 11, true);
    player.animations.add("shield_left", [16, 17, 18, 19], 11, true);
    player.animations.add("shield_right", [22, 23, 24, 25], 11, true);
    player.animations.add("speed_left", [30, 31, 32, 33], 11, true);
    player.animations.add("speed_right", [36, 37, 38, 39], 11, true);
    this.game.camera.follow(player);
    shield = false;
};

/* Function that initiates the different object groups. */
levelThree.prototype.initGroups = function () {
    "use strict";
    death = this.game.add.group();
    death.enableBody = true;
    platforms = this.game.add.group();
    platforms.enableBody = true;
    sawblades = this.game.add.group();
    sawblades.enableBody = true;
    movablePlatforms = this.game.add.group();
    movablePlatforms.enableBody = true;
    collectibles = this.game.add.group();
    collectibles.enableBody = true;
    enemies = this.game.add.group();
    enemies.enableBody = true;
};

/* Checks for collisions of the player with objects in the game. */
levelThree.prototype.updateCollisions = function () {
    "use strict";
    if (!died) {
        this.game.physics.arcade.collide(player, platforms, this.checkDoor, null, this);
        this.game.physics.arcade.collide(player, movablePlatforms);
        this.game.physics.arcade.overlap(player, goal, this.finishLevel, null, this);
        this.game.physics.arcade.collide(player, death, this.collision, null, this);
        this.game.physics.arcade.overlap(player, collectibles, this.collectCoin, null, this);
        this.game.physics.arcade.collide(player, sawblades, this.collision, null, this);
        this.game.physics.arcade.collide(player, enemies, this.collision, null, this);
    }
};

/**
Checks whether the player collected the key and is thus able to open the door, that is to say make it disappear.
It also adds stars as a reward.
@param {Object} player - the character
@param {String} door - name of the door, that is to say "door1" or "door2"
*/
levelThree.prototype.checkDoor = function (playerCurrent, door) {
    "use strict";
    if (door.name === "door1") {
        if (keycheck === true) {
            door.kill();
            this.addStars(2866, 400, 2, 2, 0);
            keycheck = false;
            keyIcon.visible = false;
        }
    } else if (door.name === "door2") {
        if (keycheck === true) {
            door.kill();
            this.addStars(5796, 400, 2, 2, 0);
            keycheck = false;
            keyIcon.visible = false;
        }
    }
};

/* Function that updates the player movement. It is called in the update() function and checks for all kinds of different states, and shows the respective animations. */
levelThree.prototype.updatePlayerMovement = function () {
    "use strict";
    if (!pose && !died) {
        cursors = this.game.input.keyboard.createCursorKeys();
        player.body.velocity.x = 0;
        if (cursors.left.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1) {
            player.body.velocity.x = -playerVelocityX;
            if (shield && !hitDeath) {
                player.animations.play("shield_left");
            } else {
                player.animations.play("left");
            }

        } else if (cursors.right.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1) {
            player.body.velocity.x = playerVelocityX;
            if (shield && !hitDeath) {
                player.animations.play("shield_right");
            } else {
                player.animations.play("right");
            }

        } else {
            player.animations.stop();
            player.frame = 5;
            if (shield && !hitDeath) {
                player.frame = 20;
            }
        }
        if (cursors.up.isDown && player.body.touching.down && !touchingCollectibles || pad1.justPressed(Phaser.Gamepad.XBOX360_A) && player.body.touching.down && !touchingCollectibles) {
            player.body.velocity.y = -playerVelocityY;
        }
        if (player.body.velocity.y < 0) {
            if (cursors.left.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1) {
                player.animations.stop();
                player.frame = 0;
                if (shield && !hitDeath) {
                    player.frame = 15;
                }
            }
            if (cursors.right.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1) {
                player.animations.stop();
                player.frame = 11;
                if (shield && !hitDeath) {
                    player.frame = 26;
                }
            }
        }
    } else if (pose && !shield) {
        player.frame = 6;
    } else if (pose && shield && !hitDeath) {
        player.frame = 21;
    } else if (pose) {
        player.frame = 35;
    } else if (died && player.body.velocity.y < 0) {
        player.frame = 12;
    } else {
        player.frame = 13;
    }
    if (hitDeath) {
        player.animations.stop();
        player.frame = 14;
    }
};

/* Function that checks whether the user entered a cheat code. */
levelThree.prototype.checkForCheats = function () {
    "use strict";
    if (this.game.input.keyboard.isDown(Phaser.Keyboard.G)) {
        playerVelocityX = 600;
        playerVelocityY = 600;
    } else if (this.game.input.keyboard.isDown(Phaser.Keyboard.K)) {
        playerVelocityX = 250;
        playerVelocityY = 350;
    }
};

/* Function that rotates the sawblades, that is to say that loads the respective animations. */
levelThree.prototype.rotateSawblades = function () {
    "use strict";
    for (var i = 0; i < sawblades.children.length; i++) {
        sawblades.getChildAt(i).animations.play("rotate");
    }
};

/* Function that is triggered when the user hits the ESC key, which then makes the game return to the title screen. */
levelThree.prototype.exitLevel = function () {
    "use strict";
    if (this.game.input.keyboard.isDown(Phaser.Keyboard.ESC) || pad1.justPressed(Phaser.Gamepad.XBOX360_START)) {
        this.game.state.start("titleScreen");
    }
};
