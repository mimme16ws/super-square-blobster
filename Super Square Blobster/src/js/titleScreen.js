var levelSelectButton,
    highscoreButton,
    helpButton;

var titleScreen = function () {
    "use strict";
};

titleScreen.prototype = {
    /* global sessionStorage
    document */
    /* Loads buttons and images */
    create: function () {
        "use strict";
        var gameTitle = this.game.add.sprite(-50, -30, "title_new");
        gameTitle.scale.setTo(1.1, 1.1);
        levelSelectButton = this.game.add.button(590, 120, "button_select", this.playTheGame, this, 1, 0);
        highscoreButton = this.game.add.button(590, 270, "button_highscores", this.highscores, this, 1, 0);
        helpButton = this.game.add.button(590, 420, "button_help", this.help, this, 1, 0);
    },


    /* Before starting the levelSelect screen, this function shows the div with an input field that the user needs to enter his name in.
        After having done so, the div's status is set to hidden.*/
    playTheGame: function () {
        "use strict";
        this.disableButtons();
        var name = document.getElementById("name");
        var nameInput = document.getElementById("nameInput");
        var close = document.getElementById("close");
        name.style.visibility = "visible";
        nameInput.focus();
        var gameCurrent = this;
        nameInput.onkeydown = function (event) {
            if (event.keyCode === 13 && nameInput.value !== "") {
                name.style.visibility = "hidden";
                gameCurrent.game.state.start("levelSelect");
                var userName = nameInput.value;
                sessionStorage.clear();
                sessionStorage.setItem("username", userName);
                nameInput.value = "";
                gameCurrent.enableButtons();
            }
        };
        close.onclick = function () {
            name.style.visibility = "hidden";
            nameInput.value = "";
            gameCurrent.enableButtons();
        };
    },

    /* Switches to the highscore screen. */
    highscores: function () {
        "use strict";
        this.game.state.start("highscoreScreen");
    },

    /* Switches to the help screen. */
    help: function () {
        "use strict";
        this.game.state.start("helpScreen");
    },

    /* Disables the buttons. */
    disableButtons: function () {
        "use strict";
        levelSelectButton.inputEnabled = false;
        highscoreButton.inputEnabled = false;
        helpButton.inputEnabled = false;
    },

    /* Enables the buttons. */
    enableButtons: function () {
        "use strict";
        levelSelectButton.inputEnabled = true;
        highscoreButton.inputEnabled = true;
        helpButton.inputEnabled = true;
    }

};
