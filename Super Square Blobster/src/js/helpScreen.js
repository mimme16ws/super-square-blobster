var helpScreen = function () {
    "use strict";
};

helpScreen.prototype = {
    /* Sends the help image and the back button to the screen.*/
    create: function () {
        "use strict";
        this.game.add.image(0, 0, "helpScreen");
        this.game.add.text(375, 30, "Hilfe", {
            fontSize: "30px",
            fill: "#fbb03b",
            stroke: "#000",
            strokeThickness: 6
        });
        var backButton = this.game.add.button(2, 2, "button_back", this.goBack, this, 1, 0);
        backButton.scale.setTo(0.8, 0.8);

    },
    goBack: function () {
        "use strict";
        this.game.state.start("titleScreen");
    }
};
